const express = require('express');
const morgan = require('morgan');
const mysql = require('mysql');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');

const indexRouter = require('./routes/index');
const loginRouter = require('./routes/login');
const registerRouter = require('./routes/register');
const todoRouter = require('./routes/todo');
// const customersRouter = require('./routes/customers');


const authTokens = require('./lib/session');

const postRoutes = require('./routes/post');
const connection = require('./lib/db');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static( 'public'));
app.use(cookieParser());
//app.use(cors());

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log("app is listening port:", port);
});

 app.use('/', indexRouter);
 app.use('/login', loginRouter);
 app.use('/register', registerRouter);
 app.use('/todo', todoRouter);
//  app.use('/customers', customersRouter);

 
app.use((req, res, next) => {
  const authToken = req.cookies['AuthToken'];
  req.user = authTokens[authToken];
  next();
});

connection.query("SELECT * FROM users", function (err, result, fields) {
  if (err) throw err;
  console.log(result);
});
//app.use('/aa', postRoutes);



app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
 });
 app.use(function(err, req, res) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;