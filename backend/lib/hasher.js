const crypto = require('crypto');

const hasher = {
    getHashedPassword: (password) => {
        const sha256 = crypto.createHash('sha256');
        return sha256.update(password).digest('base64');
    },
    generateAuthToken: () => {
        return crypto.randomBytes(30).toString('hex');
    }
};

module.exports = hasher;
