const express = require('express');

const router = express.Router();
router.get('/', (req, res) => {
    const customers = [
        { id: 1, firstName: 'Mati', secondName: 'Cuper'}
    ];
    res.json(customers);
});

module.exports = router;