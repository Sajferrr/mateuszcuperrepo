import React, { Component } from 'react';
import './Todo.css';
import { Redirect } from 'react-router-dom'
import authService from '../services/authService';
import {Button} from 'react-bootstrap/Button';

class Index extends Component {
  constructor() {
      super();
      this.state = {
        tasks: []
      }
  }
  componentDidMount() {
      fetch('/')
      .then(res => res.json())
      .then(tasks => this.setState({tasks: tasks}));

  }
  logout() {
    authService.logout();
    window.location.href='/login'

  }
    render(){
      var isUserLoggedIn = authService.isUserAuthenticated();
  return (
    <div className='main'>
    <ul>
      <li><a href="/">Main</a></li>
      <li><a href="/login">Login</a></li>
      <li><a href="/register">Register</a></li>
      <li><a href="/todo">Todo</a></li>
    </ul>
    
    {isUserLoggedIn ? 
      <button onClick={this.logout} type="button" class="btn btn-success btn-block">Logout</button>: ""
    }
<section id="top" class="jumbotron jumbotron-fluid" > 
      <div class="container"> 
        <h1 class="display-1 text-muted"><font color="white">Welcome</font></h1> 
        <p class="display-4 text-muted"><font color="white">on my website! </font></p> 
        
        <a href="/play" class="btn btn-lg btn-primary">START</a> 
      </div> 
    </section>
   
    

    <footer>
        <a href="https://www.linkedin.com/in/mateusz-cuper-front"><img src="https://img.icons8.com/color/40/000000/linkedin.png" target="_blank"></img></a>
        <p>Created by: Mateusz Cuper</p>
        <p>Contact information: <a href="mailto:matic07@interia.pl">matic07@interia.pl</a>.</p>
    </footer>
    </div>
    
    
  );
}
}
export default Index;
