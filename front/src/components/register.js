import React, { Component } from 'react';
import './Todo.css';
import auth from './auth';
import axios from "axios";
class Login extends Component {
  constructor() {
      super();
      this.state = {
        email: "",
        password: "",
        info: []

      }
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);


  }
  // componentDidMount() {
  //     fetch('/login')
  //     .then(res => res.json())
  //     .then(tasks => this.setState({tasks: tasks}));

  // };
  handleSubmit(event){
const {email, password} = this.state;
axios.post(
  "http://localhost:8080/register",
  
    {
email: email,
password: password
    }

  
).then(res => {res.json();
//   document.cookie = 'AuthToken='+ res.data.authToken;
//   auth.login();
//   localStorage.setItem('auth', JSON.stringify(auth));
  console.log(res);})
.catch(err => {
  console.log("register error", err);
});

event.preventDefault();

}

  
  handleChange(event){
    this.setState({
      [event.target.name]: event.target.value
    });
  }
    render(){
  return (
    <div className='main'>

<ul>
      <li><a href="/">Main</a></li>
      <li><a href="/register">Login</a></li>
    </ul>
                        <section id="inner-wrapper" class="login">
						<p><h2>Register</h2></p>
							<article>
								<form onSubmit={this.handleSubmit}>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-user"> </i></span>
											<input name="email" type="email" class="form-control" onChange={this.handleChange} placeholder="Enter email" value={this.state.email}/>
										</div>
									</div>
                                    <div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-user"> </i></span>
											<input name="password" type="password" class="form-control" onChange={this.handleChange} placeholder="Password" value={this.state.password}/>
										</div>
									</div>
									
									  <button type="submit" class="btn btn-success btn-block">Register</button>
								</form>
							</article>
                        </section>
                        

    <footer>
        <a href="https://www.linkedin.com/in/mateusz-cuper-front"><img src="https://img.icons8.com/color/40/000000/linkedin.png" target="_blank"></img></a>
        <p>Created by: Mateusz Cuper</p>
        <p>Contact information: <a href="mailto:matic07@interia.pl">matic07@interia.pl</a>.</p>
    </footer>
    </div>
    
    
  );
}
}
export default Login;
