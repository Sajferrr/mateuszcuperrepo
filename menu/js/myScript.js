

var btnMenu = document.getElementById("btn-menu");
btnMenu.addEventListener("click", showHideMenu);

function showHideMenu(e){
	var overlayObj = document.getElementsByClassName("overlay")[0];
	var speedMs = 10;
	
	var eventTarget = e.target;
	if(eventTarget.className == "btn-openn"){
		eventTarget.className = "btn-closee";
		fadeIn(overlayObj, speedMs);
		
	}
	else{
		eventTarget.className = "btn-openn";
		fadeOut(overlayObj, speedMs);
	}
}

function fadeIn(elem, speedMs){
	var inInterval = setInterval(function(){
		elem.style.opacity = Number(elem.style.opacity) + 0.02;
		if(elem.style.opacity >= 1){
			elem.style.opacity = 1;
			clearInterval(inInterval);
		}
	}, speedMs);
}

function fadeOut(elem, speedMs){
	var outInterval = setInterval(function(){
		elem.style.opacity = Number(elem.style.opacity) - 0.02;
		if(elem.style.opacity <= 0){
			elem.style.opacity = 0;
			clearInterval(outInterval);
		}
	}, speedMs);
}